package  
{
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import flash.events.NetStatusEvent;
	import flash.events.TimerEvent;
	import flash.media.Camera;
	import flash.media.Video;
	import flash.net.NetConnection;
	import flash.net.NetStream;
	
	import flash.utils.Timer;
	import org.casalib.util.StringUtil;
	/**
	 * ...
	 * @author xaca
	 */
	public class Prueba extends MovieClip
	{
		
		//Define a NetConnection variable nc
		private var nc:NetConnection;
		//Define two NetStream variables, ns_in and ns_out
		private var ns_in:NetStream;
		private var ns_out:NetStream;
		//Define a Camera variable cam
		private var cam:Camera;
		//Define a Video variable named vid_out
		private var vid_out:Video;
		//Define a Video variable named vid_in
		private var vid_in:Video;
		private var vid:String;
		
		private var timer:Timer;
		
		public function Prueba() 
		{			
			cam = Camera.getCamera();
			
			if (cam)
			{
				init();	
			}
			else 
			{
				trace("No tiene camara web");
			}
		}
		
		private function init():void
		{
			vid = StringUtil.uuid();
			timer = new Timer(3000);
			timer.addEventListener(TimerEvent.TIMER_COMPLETE, onGrabar);
			timer.start();
			//Create a new NetConnection by instantiating nc
			nc = new NetConnection();
			//Add an EventListener to listen for onNetStatus()
			nc.addEventListener(NetStatusEvent.NET_STATUS, onNetStatus);
			//Connect to the live folder on the server
			nc.connect("rtmp://localhost/prueba");
			//nc.connect("rtmp://YOUR_SERVER_URL/live");
			//Tell the NetConnection where the server should invoke callback methods
			//nc.client = this;
			nc.client={onMetaData:function(obj:Object):void{} }
			
			//Instantiate the vid_in variable, set its location in the UI, and add it to the stage
			vid_in = new Video();
			vid_in.x = 100; 
			vid_in.y = 100;
			addChild( vid_in );
			
			btnCancelar.addEventListener(MouseEvent.CLICK, onCancelar);
			btnVolverAGrabar.addEventListener(MouseEvent.CLICK, onVolverAGrabar);
			btnVerVideo.addEventListener(MouseEvent.CLICK, onReproducir);
		}
		
		private function onReproducir(e:MouseEvent):void 
		{
			reproducir();
		}
		
		private function onVolverAGrabar(e:MouseEvent):void 
		{
			reproducir();
			grabar();
		}
		
		private function onCancelar(e:MouseEvent):void 
		{
			trace("stop");
			ns_out.close();
		}
		
		private function onGrabar(e:TimerEvent):void 
		{
			grabar();
			timer.removeEventListener(TimerEvent.TIMER_COMPLETE, onGrabar);
		}
		
		private function onNetStatus(event:NetStatusEvent):void 
		{
			if( event.info.code == "NetConnection.Connect.Success" )
			{ 
				playComercial();
				//publicar();
				//reproducir();
			}
		}
		
		private function playComercial():void
		{
			ns_out = new NetStream(nc);
			ns_out.play("sample");
			ns_out.client = { onMetaData:function(obj:Object):void { } };
			ns_out.addEventListener(NetStatusEvent.NET_STATUS, function(stats:NetStatusEvent) {
				if (stats.info.code == 'NetStream.Play.Stop') {
					 trace('the video has ended');
					 publicar();
				     reproducir();
				}
			});
			vid_in.attachNetStream(ns_out);
		}
		
		private function publicar():void 
		{
			ns_out = new NetStream(nc);
			ns_out.attachCamera(cam);
			cam.setQuality( 90000, 90 );
			cam.setMode( 320, 240, 30, true );
			cam.setKeyFrameInterval( 15 );
			ns_out.client = { onMetaData:function(obj:Object):void { } };
			ns_out.publish(vid, "live");
		}
		
		private function reproducir():void 
		{
			ns_in = new NetStream(nc);
			ns_in.client = { onMetaData:function(obj:Object):void { } };
			ns_in.play(vid);
			vid_in.attachNetStream(ns_in);
		}
		
		private function grabar():void 
		{
			/*ns_out = new NetStream(nc);
			ns_out.attachCamera(cam);
			cam.setQuality( 90000, 90 );
			cam.setMode( 320, 240, 30, true );
			cam.setKeyFrameInterval( 15 );
			ns_out.client = this;*/
			
			ns_out.publish(vid, "record");
			
			/*var metaData:Object = new Object();
			//Give the metadata object properties to reflect the stream's metadata
			//metaData.codec = ns_out.videoStreamSettings.codec;
			//metaData.profile =  h264Settings.profile;
			//metaData.level = h264Settings.level;
			metaData.fps = cam.fps;
			metaData.bandwith = cam.bandwidth;
			metaData.height = cam.height;
			metaData.width = cam.width;
			metaData.keyFrameInterval = cam.keyFrameInterval;
			//Call send() on the ns_out NetStream
			ns_out.send( "@setDataFrame", "onMetaData", metaData );*/
		}
	}

}