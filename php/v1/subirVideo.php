<?php

	require_once 'Zend/Loader.php'; // the Zend dir must be in your include_path
	Zend_Loader::loadClass('Zend_Gdata_YouTube');
	Zend_Loader::loadClass('Zend_Gdata_ClientLogin'); 

	$rutaArchivo = $_POST['rutaVideo'];
	$authenticationURL= 'https://www.google.com/accounts/ClientLogin';
	$httpClient = 
	  Zend_Gdata_ClientLogin::getHttpClient(
				  $username = '',
				  $password = '',
				  $service = 'youtube',
				  $client = null,
				  $source = 'MySource', // a short string identifying your application
				  $loginToken = null,
				  $loginCaptcha = null,
				  $authenticationURL);

	//$yt = new Zend_Gdata_YouTube($httpClient);
	$developerKey = 'AI39si550B0pAbdv_rQo4Lb70ijsrhJRNOT5av84oDIAb3vYBLRJTV1cCKHBNngMYW8EB2kotyYI__L5MQcDN7CoVlIFOB3kMQ';
	$applicationId = '';
	$clientId = '';
	
	$yt = new Zend_Gdata_YouTube($httpClient, $applicationId, $clientId, $developerKey);
	
	$myVideoEntry = new Zend_Gdata_YouTube_VideoEntry();
	 
	$filesource = $yt->newMediaFileSource(''.$rutaArchivo);
	//$filesource->setContentType('video/quicktime');
	$filesource->setContentType('video/x-flv');
	$filesource->setSlug('sample.flv');
	 
	$myVideoEntry->setMediaSource($filesource);
	 
	$myVideoEntry->setVideoTitle('My Test Movie');
	$myVideoEntry->setVideoDescription('My Test Movie');
	// Note that category must be a valid YouTube category !
	$myVideoEntry->setVideoCategory('Comedy');
	 
	// Set keywords, note that this must be a comma separated string
	// and that each keyword cannot contain whitespace
	$myVideoEntry->SetVideoTags('cars, funny');
	 
	// Optionally set some developer tags
	$myVideoEntry->setVideoDeveloperTags(array('mydevelopertag',
											   'anotherdevelopertag'));
	/* 
	// Optionally set the video's location
	$yt->registerPackage('Zend_Gdata_Geo');
	$yt->registerPackage('Zend_Gdata_Geo_Extension');
	$where = $yt->newGeoRssWhere();
	$position = $yt->newGmlPos('37.0 -122.0');
	$where->point = $yt->newGmlPoint($position);
	$myVideoEntry->setWhere($where);
	*/
	 
	// Upload URI for the currently authenticated user
	$uploadUrl =
		'http://uploads.gdata.youtube.com/feeds/users/default/uploads';
	 
	// Try to upload the video, catching a Zend_Gdata_App_HttpException
	// if availableor just a regular Zend_Gdata_App_Exception
	 
	try {
		$newEntry = $yt->insertEntry($myVideoEntry,
									 $uploadUrl,
									 'Zend_Gdata_YouTube_VideoEntry');
		
		$newEntry->setVideoPublic();
		// Assuming that $videoEntry is the object that was returned during the upload
		$state = $newEntry->getVideoState();
		$ans = array();
		if ($state) {
			$ans['error'] = false;
			$ans['vid'] = $newEntry->getVideoId();
			$ans['nombre'] = $state->getName();
			$ans['texto'] = $state->getText();
			$ans['fecha'] = $newEntry->getUpdated();
			
		} else {
			$ans['error'] = true;
			$ans['des'] = "Not able to retrieve the video status information yet. " . "Please try again later.\n";
		}
		die(json_encode($ans));
		
	} catch (Zend_Gdata_App_HttpException $httpException) {
		echo $httpException->getRawResponseBody();
	} catch (Zend_Gdata_App_Exception $e) {
		echo $e->getMessage();
	}

?>