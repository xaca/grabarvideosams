  /* Funciones FACEBOOK */
  var abrirRegistro = false;
  var logeado = false;
  
  function pintarDatosFB()
  {
	  FB.api('/me', function(response) {
	  //{"id": "220439","name": "Bret Taylor","first_name": "Bret","last_name": "Taylor","link": "http://www.facebook.com/btaylor","username": "btaylor","gender": "male","locale": "en_US"}
	  	$("#uid").val(response.id);
		$("#nombre").val(response.first_name);
		$("#apellido").val(response.last_name);
		location.href = "#registros";
	  });
  }
    
  function login() {
    FB.login(function(response) {
        if (response.authResponse) {
            // connected
			abrirRegistro = true;
			estaRegistrado(response.authResponse.userID);
        } else {
            // cancelled
        }
    });
  }
  
  function validarFacebook()
  {
	  FB.getLoginStatus(function(response) {
		  if (response.status === 'connected') {
			// connected
			//response-> { status: 'connected',authResponse: {accessToken: '...',expiresIn:'...',signedRequest:'...',userID:'...'}
			abrirRegistro = true;
			estaRegistrado(response.authResponse.userID);			
		  } else if (response.status === 'not_authorized') 
		  {
			// not_authorized
			login();
		  } else 
		  {
			// not_logged_in
			login();
		  }
	 });
  }
  
  
  //Interacción con la base de datos
  function procesarDatos(comando,params){
	  	//alert(JSON.stringify(params));
        $.ajax({
          type: "POST",
          url: "./php/libreria.php?cmd="+comando,
          data: params,
          dataType: "json",
		  success:function( datos ) {
            respuestaExito(datos,comando);
          },
		  error:function( datos ) {
            respuestaError(datos,comando);
          }
        });
    }
 
    function respuestaExito(datos,cmd){
		//alert(JSON.stringify(datos));
		if(!datos.error)
		{			
			if(cmd == "insertar")
			{
				mostrarMensaje("Yeahh!!!","Muchas gracias por su registro.","exito");
				mostrarBotonesRegistro(false);
			}
			
			if(cmd == "estaRegistrado")
			{
				if(datos.registrado >= 1)
				{
					mostrarMensaje(":)","Ya se encuentra registrado","exito");
					mostrarBotonesRegistro(false);
				}
				else
				{
					if(abrirRegistro)
					{
						abrirRegistro = false;
						pintarDatosFB();
					}
				}
			}
		}
		else
		{
			mostrarMensaje("Oops!","Error al realizar el registro, intente nuevamente más tarde","error");
		}
	}
    
	function mostrarBotonesRegistro(flag)
	{
		if(flag)
		{
			$("#botoneRegistro").show();
		}
		else
		{
			$("#botoneRegistro").hide();
		}
	}
	
	 function respuestaError(datos,cmd){
		//if(datos.error)
		//{
			if(cmd == "insertar")
			{
				mostrarMensaje("Oops!","Error al realizar el registro, intente nuevamente más tarde","error");
			}
		//}
	}
   
    function insertarDatos(){
		var uid = $("#uid").val();
        var nombre = $("#nombre").val();
        var apellido = $("#apellido").val();
        var email = $("#email").val();
		var celular = $("#celular").val();
		var intereses = $.map($('input:checkbox:checked'), function(e,i) {
			return +e.value;
	    });
				
        procesarDatos("insertar",{uid:uid,nombre:nombre,apellido:apellido,email:email,celular:celular,intereses:intereses.join(","),activo:1,curso:"programaci&oacute;n para dise&ntilde;adores"});
    }
	
	function estaRegistrado(uid)
	{
		//var uid = $("#uid").val();
		procesarDatos("estaRegistrado",{uid:uid});
	}
	
  /* Funciones VALIDACIÓN */
  
  function esCorreo(id)
  {
	    var campo = $("#"+id).val();
		var emailPattern = /^[a-zA-Z0-9._]+[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-zA-Z]{2,4}$/;  
    	return emailPattern.test(campo);   
  
  }
  
  function esVacio(id)
  {
	  var campo = $("#"+id).val();
	  if (campo==null || campo=="")
	  {
	  	//alert("First name must be filled out");
	  	return true;
	  }
	  return false;
  }
    
  function esNumero(id)
  {
	   var campo = $("#"+id).val();
       var numbers = /^[0-9]+$/;
	   	   
	   //if (campo.length > 0 && campo.match(numbers))
	   if (campo.length == 10 && campo.match(numbers))//valida los 10 numeros de un celular
	   {
		   return true;
	   }
	   return false;
  }
  
  function seleccionoCheckBox(){
  	 var ans = $.map($('input:checkbox:checked'), function(e,i) {
			return +e.value;
	 });
	 return ans.length > 0;
  }
  
  function validarFormulario()
  {
	  var msgError = new Array();
	  
	  if(!esCorreo("email"))
	  {
		  msgError.push("Ingrese un correo valido");
	  }
	  
	  if(esVacio("nombre"))
	  {
		  msgError.push("El nombre es obligatorio");
	  }
	  
	  if(esVacio("apellido"))
	  {
		  msgError.push("El apellido es obligatorio");
	  }
	  
	  if(!esNumero("celular"))
	  {
		  msgError.push("El celular es obligatorio y debe tener 10 digitos");
	  }
	  if(!seleccionoCheckBox())
	  {
		  msgError.push("Seleccione por lo menos un tema de estudio");
	  }
	  
	  if(msgError.length>0)
	  {	
		mostrarMensaje("Oops!!",msgError.join("<br>"),"error");
	  }
	  else
	  {
		  insertarDatos();
	  }
  }
  
  function mostrarMensaje(titulo,texto,tipo)
  {
	
	if(tipo == "exito")
	{
		$("#botonesMensaje").html('<a href="#infoCurso" data-direction="reverse" data-role="button" data-theme="a" >Aceptar</a>');
	}
	if(tipo == "error")
	{
		$("#botonesMensaje").html('<a href="#" data-rel="back" data-theme="a" data-role="button" >Aceptar</a>');
	}
	$("#tituloMensaje").html(titulo);
	$("#errorFormulario").html(texto);
	$('#dialog').trigger("create");
	$.mobile.changePage('#dialog', 'pop', true, true);
  }